jQuery(document).ready(function($) {

    // Scrollto
    $('a[href^="#"]').on('click', function(event) {
        var target = $( $(this).attr('href') );
        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 500);
        }
    });

    $('.bgjs').each(function(index, el) {
        $(this).css('background-image','url('+$(this).find('img').attr('src')+')');
    });

    // LightBox
    $('.video-gallery').lightGallery({
        selector: '.item-video',
        youtubePlayerParams: {
            autoplay: 1,
            modestbranding: 1,
            showinfo: 1,
            rel: 0,
            controls: 1
        }
    });

    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

    $('.mask-tel').mask(SPMaskBehavior, spOptions);
    $('.mask-cpf').mask('999.999.999-99');
    $('.mask-cep').mask('99999-999');
    $('.mask-data').mask('99/99/9999');
  
    // Cadastro
    $(".form-cadastro").validate({

        submitHandler: function(form) {

            var $form  = $(form);

            $form.find('button').html('<i class="fa fa-refresh fa-spin"></i>').attr({
                type: 'button',
                disabled: 'disabled'
            });

            $.ajax({
                url: 'cadastro.php',
                type: 'POST',
                dataType: 'json',
                data: $form.serialize()
            })
            .always(function(data) {
                $form.find('button').html('OK').attr({
                    type: 'submit'
                }).removeAttr('disabled');

                if (data.class == "alert-danger") {
                    alert(data.messenge);
                } else {
                    $form.find('input').val("");
                    $form.slideUp();
                    $form.parents('.modal-body').find('.resposta').removeClass('hide').addClass(data.class).html(data.messenge);
                }
            });
            
        }

    });

});
