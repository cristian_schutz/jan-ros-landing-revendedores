
	var gulp = require('gulp');

	var autoprefixer = require('gulp-autoprefixer');
	var concat = require('gulp-concat');
	var less = require('gulp-less');
	var liveReload  = require('gulp-livereload');
	var minifyCSS = require('gulp-minify-css');
	var rename = require('gulp-rename');
	var uglify = require('gulp-uglify');
	var watch = require('gulp-watch');
	var flatten = require('gulp-flatten');
	var browserSync = require('browser-sync').create();
	var reload      = browserSync.reload;


	 browserSync.init({
	      open: 'local',
	      host: 'localhost',
	      proxy: 'localhost',
	      port: 8080 // for work mamp
	});
	// acesse http://dev.redirectdigital.com.br:8080/utp para usar


    var jsMainFiles =     	[
	                        'bower_components/jquery/dist/jquery.min.js',
	                        'bower_components/bootstrap/dist/js/bootstrap.min.js',
	                        'bower_components/Slidebars/dist/slidebars.js',
	                        'bower_components/jquery-cycle2/build/jquery.cycle2.js',
	                        'bower_components/jquery-cycle2/build/plugin/jquery.cycle2.swipe.min.js',
	                        'bower_components/jquery-validation/dist/jquery.validate.js',
	                        'bower_components/jquery-mask-plugin/dist/jquery.mask.min.js',
	                        'bower_components/lightgallery/dist/js/lightgallery-all.js',
	                        'bower_components/lightgallery/lib/jquery.mousewheel.min.js',
	                        'bower_components/matchHeight/dist/jquery.matchHeight-min.js',
	                        'bower_components/owl.carousel/dist/owl.carousel.js',
	                        'bower_components/wow/dist/wow.js',
	                        'js/*',
                   		];

    var cssMainFiles =   	[	
    						'bower_components/bootstrap/dist/css/bootstrap.css',
    						'bower_components/font-awesome/css/font-awesome.min.css',
    						'bower_components/lightgallery/dist/css/lightgallery.min.css',
    						'bower_components/wow/css/libs/animate.css',
    						'bower_components/Slidebars/dist/slidebars.css',
	                        'bower_components/owl.carousel/dist/assets/owl.carousel.css',
    						'css/all.css'
    					];

	var fontsFiles = 	[
							'bower_components/font-awesome/fonts/**',
							'bower_components/bootstrap/dist/fonts/**',
							'bower_components/lightgallery/dist/fonts/**'
						];


	// COPIA AS FONTS DO BOWER
	function copyBowerFonts(){
		return gulp.src(fontsFiles)
	  	.pipe(flatten())
	  	.pipe(gulp.dest('../_assets/fonts/'));
	}
	gulp.task('copy-bower-fonts', gulp.series(copyBowerFonts));



	// MAIN
	function lessMain() {
		return gulp.src('less/import.less')
			.pipe(less())
			.pipe(rename('all.css'))
			.pipe(gulp.dest('css'));
	}
	function cssMain() {
		return gulp.src(cssMainFiles)
	        .pipe(concat('all.min.css'))
	        .pipe(gulp.dest('../_assets/css/'))
	        .pipe(minifyCSS({compatibility: 'ie8'}))
	        .pipe(gulp.dest('../_assets/css/'))
	        .pipe(browserSync.reload({stream:true}));
	}
	function scripts(){
		return gulp.src(jsMainFiles)
			.pipe(concat('all.min.js'))
			.pipe(gulp.dest('../_assets/js/'))
			.pipe(uglify())
			.pipe(gulp.dest('../_assets/js/'))
	        .pipe(liveReload({auto: false}));
	}
	gulp.task('js', scripts);
	gulp.task('main', gulp.series(lessMain,cssMain));



	// ADMIN
	function lessAdmin() {
		return gulp.src('less/admin.less')
			.pipe(less())
			.pipe(rename('admin.css'))
			.pipe(gulp.dest('css'));
	}
	function cssAdmin() {
	  	return gulp.src(['bower_components/font-awesome/css/font-awesome.css', 'css/admin.css'])
        .pipe(concat('admin.min.css'))
        .pipe(gulp.dest('../_assets/css/'))
        .pipe(browserSync.reload({stream:true}));
	}
	function jsAdmin() {
	  	return gulp.src(['bower_components/bootstrap/dist/js/bootstrap.min.js','bower_components/datatables.net/js/jquery.dataTables.min.js'])
        .pipe(concat('admin.min.js'))
		.pipe(gulp.dest('../_assets/js/'))
		.pipe(uglify())
		.pipe(gulp.dest('../_assets/js/'))
        .pipe(liveReload({auto: false}));
	}
	gulp.task('adminjs', jsAdmin);
	gulp.task('admin', gulp.series(lessAdmin,cssAdmin));



	gulp.task('all', gulp.parallel('main', 'admin'));


	gulp.task('watch', function() {
		gulp.watch('less/*.less', gulp.parallel('main')),
		gulp.watch('css/admin.css', gulp.parallel('admin'));
		gulp.watch('js/*.js', gulp.parallel('js'));
	});

	// tasks globais
	gulp.task(
		'default',
	  	gulp.series(
			'copy-bower-fonts',
			'js',
	  		gulp.parallel(
				'main',
				'admin'
			)
	  	)
	);
	


	// gulp.task('lessadmin', function() {

	// 	gulp.src('source/less/admin.less')
	// 		.pipe(less())
	// 		.pipe(rename('admin.css'))
	// 		.pipe(gulp.dest('source/css'));

	// });
	// gulp.task('cssadmin', function() {
	// 	gulp.src(cssFilesAdmin)
	//         .pipe(concat('admin.min.css'))
	//         .pipe(gulp.dest(cssDest))
	//         .pipe(minifyCSS({compatibility: 'ie8'}))
	//         .pipe(gulp.dest(cssDest));
	// });



	// gulp.task('prefix', function () {
	// 	gulp.src("source/css/less.css")
	// 	.pipe(autoprefixer({
	// 		browsers: ['last 4 versions'],
	// 		cascade: false
	// 	}))
	// 	.pipe(gulp.dest('/source/css/'))
	// 	.pipe(liveReload({auto: false}));
	// });

	// gulp.task('js', function() {
		
	// 	gulp.src(jsFiles)
	// 		.pipe(concat('all.min.js'))
	// 		.pipe(gulp.dest(jsDest))
	// 		.pipe(uglify())
	// 		.pipe(gulp.dest(jsDest))
	//         .pipe(liveReload({auto: false}));

	// 	gulp.src(jsFilesAdmin)
	// 		.pipe(concat('admin.min.js'))
	// 		.pipe(gulp.dest(jsDest))
	// 		.pipe(uglify())
	// 		.pipe(gulp.dest(jsDest))
	//         .pipe(liveReload({auto: false}));

	// });

	// gulp.task('watch', function() {
	//     liveReload.listen();
	//     watch(jsFiles, function() {
	//         gulp.start('js');
	//     });
	//     watch('source/less/*.less', function() {
	//         gulp.start('less', 'css' );
	//     });
	// });

	// gulp.task('default', function() {
	// 	gulp.start('copy-bower-fonts','less', 'css', 'js');
	// });
